package ru.t1.dkozyaikin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME ="task-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update task by id.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getAuthService().getUserId();
        @NotNull final Task task = getTaskService().updateById(userId, id, name, description);
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
