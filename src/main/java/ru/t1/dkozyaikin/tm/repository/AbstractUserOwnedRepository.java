package ru.t1.dkozyaikin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final List<M> result = findAll(userId);
        models.removeAll(result);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        @NotNull List<M> result = findAll(userId);
        if (comparator == null) return result;
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Nullable
    @Override
    public M removeById(@Nullable String userId, @Nullable String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable String userId, @NotNull Integer index) {
        @NotNull final M model = findOneByIndex(userId, index);
        return remove(model);
    }

    @Nullable
    @Override
    public M add(@Nullable String userId, @Nullable M model) {
        if (userId == null || userId.isEmpty() || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable String userId, @Nullable M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

}
