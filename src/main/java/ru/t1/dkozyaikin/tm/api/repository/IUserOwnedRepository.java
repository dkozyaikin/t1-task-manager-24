package ru.t1.dkozyaikin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws AbstractException;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@Nullable String userId, @NotNull Integer index) throws AbstractException;

    @Nullable
    Integer getSize(@Nullable String userId) throws AbstractException;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M removeByIndex(@Nullable String userId, @NotNull Integer index) throws AbstractException;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws AbstractException;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws AbstractException;

}
