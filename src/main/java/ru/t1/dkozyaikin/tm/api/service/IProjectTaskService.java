package ru.t1.dkozyaikin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws AbstractException;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws AbstractException;

    void removeProjectByIndex(@Nullable String userId, @Nullable Integer projectIndex) throws AbstractException;

    void clearAllProjects(@Nullable String userId) throws AbstractException;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws AbstractException;

}

